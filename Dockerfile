FROM maven:3-openjdk-15 as plugin-build

WORKDIR /tmp/keycloak

COPY ./plugin .

RUN mvn package


FROM jboss/keycloak:15.0.2

RUN sed -i -e 's/<web-context>auth<\/web-context>/<web-context>\/<\/web-context>/' $JBOSS_HOME/standalone/configuration/standalone.xml
RUN sed -i -e 's/<web-context>auth<\/web-context>/<web-context>\/<\/web-context>/' $JBOSS_HOME/standalone/configuration/standalone-ha.xml

RUN sed -i -e 's/forceBackendUrlToFrontendUrl" value="false/forceBackendUrlToFrontendUrl" value="true/' $JBOSS_HOME/standalone/configuration/standalone.xml
RUN sed -i -e 's/forceBackendUrlToFrontendUrl" value="false/forceBackendUrlToFrontendUrl" value="true/' $JBOSS_HOME/standalone/configuration/standalone-ha.xml

USER root
COPY --from=plugin-build /tmp/keycloak/target/update-username-*.jar /opt/jboss/keycloak/standalone/deployments/
RUN chown -R jboss:root /opt/jboss/keycloak/standalone/deployments/ 
USER jboss

COPY ./theme /opt/jboss/keycloak/themes/m18