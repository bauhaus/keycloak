package com.gitlab.bauhaus;

import org.keycloak.Config;
import org.keycloak.authentication.RequiredActionFactory;
import org.keycloak.authentication.RequiredActionProvider;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class UpdateUsernameFactory implements RequiredActionFactory {

    private static final UpdateUsername SINGLETON = new UpdateUsername();

    @Override
    public String getDisplayText() {
        return "Change Username";
    }

    @Override
    public RequiredActionProvider create(KeycloakSession keycloakSession) {
        return SINGLETON;
    }

    @Override
    public void init(Config.Scope scope) {

    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {

    }

    @Override
    public void close() {

    }

    @Override
    public String getId() {
        return UpdateUsername.PROVIDER_ID;
    }
}
