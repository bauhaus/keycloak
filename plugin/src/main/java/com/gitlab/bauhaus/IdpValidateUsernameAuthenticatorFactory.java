package com.gitlab.bauhaus;

import org.keycloak.Config;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.idm.IdentityProviderRepresentation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IdpValidateUsernameAuthenticatorFactory implements AuthenticatorFactory {

    public static final String PROVIDER_ID = "idp-validate-username";
    static IdpValidateUsernameAuthenticator SINGLETON = new IdpValidateUsernameAuthenticator();

    public static final String UPDATE_USERNAME_ON_FIRST_LOGIN = "update.username.on.first.login";

    @Override
    public Authenticator create(KeycloakSession session) {
        return SINGLETON;
    }

    @Override
    public void init(Config.Scope config) {

    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {

    }

    @Override
    public void close() {

    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public String getReferenceCategory() {
        return "idpValidateUsername";
    }

    @Override
    public String getDisplayType() {
        return "Validate username";
    }

    @Override
    public String getHelpText() {
        return "Validates username after profile update.";
    }

    @Override
    public boolean isUserSetupAllowed() {
        return false;
    }

     @Override
    public boolean isConfigurable() {
        return false;
    }


    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return REQUIREMENT_CHOICES;
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return null;
    }

}
