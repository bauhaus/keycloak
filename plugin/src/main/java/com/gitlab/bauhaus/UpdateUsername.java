package com.gitlab.bauhaus;

import org.keycloak.authentication.RequiredActionContext;
import org.keycloak.authentication.RequiredActionProvider;
import org.keycloak.common.util.Time;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.models.UserModel;
import org.keycloak.models.RealmModel;
import org.keycloak.models.FederatedIdentityModel;
import org.keycloak.models.UserProvider;
import org.keycloak.forms.login.freemarker.model.ProfileBean;
import org.keycloak.authentication.requiredactions.util.UpdateProfileContext;
import org.keycloak.authentication.requiredactions.util.UserUpdateProfileContext;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class UpdateUsername implements RequiredActionProvider {

    public static final String PROVIDER_ID = "update_username";

    @Override
    public void evaluateTriggers(RequiredActionContext requiredActionContext) {}

    @Override
    public void requiredActionChallenge(RequiredActionContext context) {
        UpdateProfileContext userBasedContext = new UserUpdateProfileContext(context.getRealm(), context.getUser());
        Response challenge = context.form()
            .setAttribute("user", new ProfileBean(userBasedContext, null))
            .setInfo("changeUsernameMessage") // TODO: use warning
            .createForm("login-update-username.ftl");
        context.challenge(challenge);
    }

    @Override
    public void processAction(RequiredActionContext context) {
        UpdateProfileContext userBasedContext = new UserUpdateProfileContext(context.getRealm(), context.getUser());
        String newUsername = context.getHttpRequest().getDecodedFormParameters().get("username").get(0); // TODO: getFirst

        // validate new username
        List<FormMessage> errors = UsernameValidator.validate(
            newUsername,
            context.getUser(),
            context.getSession(), 
            context.getRealm()
        );

        // enforce username change
        if (newUsername.equals(context.getUser().getUsername())) {
            errors.add(new FormMessage("username", "usernameNotAllowedMessage"));
        }

        // check if username is already in use
        if (context.getSession().users().getUserByUsername(newUsername, context.getRealm()) != null) {
            errors.add(new FormMessage("username", "usernameExistsMessage"));
        }

        // prompt user for other username if errors exist
        if(!errors.isEmpty()) {
            Response challenge = context.form()
                .setAttribute("user", new ProfileBean(userBasedContext, context.getHttpRequest().getDecodedFormParameters()))
                .setInfo("changeUsernameMessage") // TODO: use warning
                .setErrors(errors)
                .createForm("login-update-username.ftl");
            context.challenge(challenge);
            return;
        }

        context.getUser().setUsername(newUsername);
        context.success();
    }

    @Override
    public void close() {}
}
