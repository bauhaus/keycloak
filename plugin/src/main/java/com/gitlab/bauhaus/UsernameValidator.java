package com.gitlab.bauhaus;

import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.FederatedIdentityModel;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.authentication.authenticators.broker.util.SerializedBrokeredIdentityContext;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

public class UsernameValidator {

  public static List<FormMessage> validate(String username, UserModel user, KeycloakSession session, RealmModel realm) {
    List<String> exceptions = new ArrayList<>();
    for (FederatedIdentityModel identity : session.users().getFederatedIdentities(user, realm)) {
        exceptions.add(identity.getUserName());    
    }

    return validate(username, exceptions);
  }

  public static List<FormMessage> validate(String username, SerializedBrokeredIdentityContext userCtx) {
    List<String> exceptions = Arrays.asList(userCtx.getBrokerUsername());

    return validate(username, exceptions);
  }

  public static List<FormMessage> validate(String username, List<String> exceptions) {
    List<FormMessage> errors = new ArrayList<>();

    if (exceptions.contains(username)) {
      return errors;
    }

    String validationRegexp = System.getenv("KEYCLOAK_USERNAME_REGEX_POLICY");

    if (username == null || username.isEmpty()) {
      errors.add(new FormMessage("username", "missingUsernameMessage"));
    } else if (validationRegexp != null && username.matches(validationRegexp)) {
      errors.add(new FormMessage("username", "usernameInvalidRegexMessage"));
    } else if (!username.matches("[a-zA-Z0-9\\.]+")) {
      errors.add(new FormMessage("username", "usernameInvalidCharacterMessage"));
    } else if (!username.matches("[a-zA-Z0-9][a-zA-Z0-9\\.]*[a-zA-Z0-9]")) {
      errors.add(new FormMessage("username", "usernameInvalidStartOrEndCharacterMessage"));
    } else if (username.matches("[0-9\\.]+")) {
      errors.add(new FormMessage("username", "usernameNoLetterMessage"));
    } else if (username.matches(".*\\.{2,}.*")) {
      errors.add(new FormMessage("username", "usernameNoMultiDotMessage"));
    } else if (username.length() < 2) {
      errors.add(new FormMessage("username", "usernameToShortMessage"));
    }

    return errors;
  }

}