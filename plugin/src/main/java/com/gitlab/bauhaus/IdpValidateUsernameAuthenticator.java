package com.gitlab.bauhaus;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.authenticators.broker.util.SerializedBrokeredIdentityContext;
import org.keycloak.broker.provider.BrokeredIdentityContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.sessions.AuthenticationSessionModel;
import org.keycloak.models.utils.FormMessage;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;


public class IdpValidateUsernameAuthenticator extends AbstractIdpAuthenticator {

    private static final Logger logger = Logger.getLogger(IdpValidateUsernameAuthenticator.class);

    @Override
    public boolean requiresUser() {
      return false;
    }

    @Override
    protected void authenticateImpl(AuthenticationFlowContext context, SerializedBrokeredIdentityContext userCtx, BrokeredIdentityContext brokerContext) {
      String username = userCtx.getUsername();

      List<FormMessage> errors = UsernameValidator.validate(username, Arrays.asList(userCtx.getBrokerUsername()));

      if (!errors.isEmpty()) {
        AuthenticationSessionModel authSession = context.getAuthenticationSession();
        context.resetFlow(() -> {
            userCtx.saveToAuthenticationSession(authSession, BROKERED_CONTEXT_NOTE);
            authSession.setAuthNote(INVALID_USERNAME, "true");
        });
      } else {
        context.success();
      }
    }

    @Override
    protected void actionImpl(AuthenticationFlowContext context, SerializedBrokeredIdentityContext userCtx, BrokeredIdentityContext brokerContext) {
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
      return true;
    }

}
