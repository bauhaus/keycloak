package com.gitlab.bauhaus;

import org.jboss.logging.Logger;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.authenticators.broker.util.SerializedBrokeredIdentityContext;
import org.keycloak.broker.provider.BrokeredIdentityContext;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.IdentityProviderModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.services.messages.Messages;
import org.keycloak.forms.login.freemarker.model.ProfileBean;
import org.keycloak.authentication.requiredactions.util.UpdateProfileContext;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class IdpReviewUsernameAuthenticator extends AbstractIdpAuthenticator {

    private static final Logger logger = Logger.getLogger(IdpReviewUsernameAuthenticator.class);

    @Override
    public boolean requiresUser() {
        return false;
    }

    @Override
    protected void authenticateImpl(AuthenticationFlowContext context, SerializedBrokeredIdentityContext userCtx, BrokeredIdentityContext brokerContext) {
        String enforceUpdateProfile = context.getAuthenticationSession().getAuthNote(ENFORCE_UPDATE_PROFILE);
        if (Boolean.parseBoolean(enforceUpdateProfile)) {
            context.success();
            return;
        }

        IdentityProviderModel idpConfig = brokerContext.getIdpConfig();
        logger.debugf("Identity provider '%s' requires update username action for broker user '%s'.", idpConfig.getAlias(), userCtx.getUsername());

        // Check if username was marked as invalid in a later step.
        List<FormMessage> errors = new ArrayList<>();
        String usernameInvalid = context.getAuthenticationSession().getAuthNote(INVALID_USERNAME);
        if (Boolean.parseBoolean(usernameInvalid)) {
            errors = UsernameValidator.validate(userCtx.getUsername(), Arrays.asList(userCtx.getBrokerUsername()));
        }

        MultivaluedMap<String, String> formData = new MultivaluedHashMap<String, String>();
        formData.putSingle("username", userCtx.getUsername());

        if (errors.size() > 0) {
            Response challengeResponse = context.form()
                .setAttribute("user", new ProfileBean((UpdateProfileContext) userCtx, formData))
                .setFormData(formData)
                .setErrors(errors)
                .createForm("login-update-username.ftl");
            context.challenge(challengeResponse);
        } else {
            Response challengeResponse = context.form()
                .setAttribute("user", new ProfileBean((UpdateProfileContext) userCtx, formData))
                .setFormData(formData)
                .setErrors(errors)
                .setInfo("reviewUsernameMessage")
                .createForm("login-update-username.ftl");
            context.challenge(challengeResponse);
        }
    }

    @Override
    protected void actionImpl(AuthenticationFlowContext context, SerializedBrokeredIdentityContext userCtx, BrokeredIdentityContext brokerContext) {
        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();

        String username = formData.getFirst("username");

        List<FormMessage> errors = UsernameValidator.validate(username, Arrays.asList(userCtx.getBrokerUsername()));

        if(errors.size() > 0) {
            Response challenge = context.form()
                .setErrors(errors)
                .setAttribute("user", new ProfileBean((UpdateProfileContext) userCtx, formData))
                .setFormData(formData)
                .createForm("login-update-username.ftl");
            context.challenge(challenge);
            return;
        }

        userCtx.setUsername(username);
        userCtx.saveToAuthenticationSession(context.getAuthenticationSession(), BROKERED_CONTEXT_NOTE);

        logger.debugf("Username updated successfully after first authentication with identity provider '%s' for broker user '%s'.", brokerContext.getIdpConfig().getAlias(), userCtx.getUsername());

        context.getAuthenticationSession().setAuthNote(INVALID_USERNAME, "false");
        context.success();
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

}
